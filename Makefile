BUNDLE = lv2-example-sustain.lv2
INSTALL_DIR = /usr/local/lib/lv2

#export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/opt/kxstudio/lib/pkgconfig/


$(BUNDLE): manifest.ttl sustain.ttl sustain_presets.ttl sustain.so sustain_ui.so
	rm -rf $(BUNDLE)
	mkdir $(BUNDLE)
	cp $^ $(BUNDLE)

sustain.so: suslv2.cpp suslv2.h sustainer.cpp sustainer.h
	g++ -shared -fPIC -DPIC suslv2.cpp sustainer.cpp -o sustain.so
	
sustain_ui.so: sustain_gui_main.cxx sustain_ui.h sliderW.h
	g++ -shared -fPIC -DPIC sliderW.cxx sustain_ui.cxx sustain_gui_main.cxx `pkg-config --cflags --libs ntk ` -o sustain_ui.so

#   DEBUG
#	g++ -g3 -ggdb -Wall -Wextra -shared -fPIC -DPIC sliderW.cxx sustain_ui.cxx sustain_gui_main.cxx `pkg-config --cflags --libs ntk ` -o sustain_ui.so
	

install: $(BUNDLE)
	mkdir -p $(INSTALL_DIR)
	rm -rf $(INSTALL_DIR)/$(BUNDLE)
	cp -R $(BUNDLE) $(INSTALL_DIR)

clean:
	rm -rf $(BUNDLE) sustain.so sustain_ui.so


# valgrind jalv -s "http://gitlab.com/stazed/lv2-example#sustainer"
