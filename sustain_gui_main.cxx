#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/x.H>

#include "sustain_ui.h"


static LV2UI_Handle init_sustainUI(const struct _LV2UI_Descriptor * descriptor,
		const char * plugin_uri,
		const char * bundle_path,
		LV2UI_Write_Function write_function,
		LV2UI_Controller controller,
		LV2UI_Widget * widget,
		const LV2_Feature * const * features) 
{
    if (strcmp(plugin_uri, SUSTAINLV2_URI) != 0)
    {
      fprintf(stderr, "SUSTAIN_URI error: this GUI does not support plugin with URI %s\n", plugin_uri);
      return NULL;
	}
    

    SustainUI* self = new SustainUI();
    if(!self) return 0;
    LV2UI_Resize* resize = NULL;

    self->controller = controller;
    self->write_function = write_function;

    void* parentXwindow = 0;
    for (int i = 0; features[i]; ++i)
    {
        if (!strcmp(features[i]->URI, LV2_UI__parent)) 
	{
           parentXwindow = features[i]->data;
        }
	else if (!strcmp(features[i]->URI, LV2_UI__resize)) 
	{
           resize = (LV2UI_Resize*)features[i]->data;
        }

    } 

    self->ui = self->show();
    fl_open_display();
    // set host to change size of the window
    if (resize)
    {
      resize->ui_resize(resize->handle, self->ui->w(), self->ui->h());
    }
    fl_embed( self->ui,(Window)parentXwindow);
    *widget = (LV2UI_Widget)fl_xid_(self->ui);

    return (LV2UI_Handle)self;
}

void cleanup_sustainUI(LV2UI_Handle ui)
{
    SustainUI *self = (SustainUI*)ui;

    delete self->ui;
    delete self;
}

void sustainUI_port_event(LV2UI_Handle ui, uint32_t port_index, uint32_t buffer_size, uint32_t format, const void * buffer)
{

    SustainUI *self = (SustainUI*)ui;
    if(!format)
    {
        float value =  *(float *)buffer;
        fprintf(stderr, "sustainUI_port_event: port_index %d: val %f\n", port_index, value);
        switch(port_index)
        {
            case PARAM1:
            {
                // the value is opposite
                int val = (int) value;
                if(val == 1)
                {
                    val = 0;
                    self->sus_activar->label("Off");
                }
                else
                {
                    val = 1;
                    self->sus_activar->label("On");
                }
                self->sus_activar->value((int)val);
            }
            break;
            case PARAM2:
              self->sus_gain->value((int)value);
            break;
            case PARAM3:
              self->sus_sus->value((int)value);
            break;
        }
    }
}

static int
idle(LV2UI_Handle handle)
{
  SustainUI* self = (SustainUI*)handle;
  self->idle();
  
  return 0;
}

static int
resize_func(LV2UI_Feature_Handle handle, int w, int h)
{
  SustainUI* self = (SustainUI*)handle;
  if(self && w>0 && h>0)
      self->ui->size(w,h);
  
  return 0;
}

static const LV2UI_Idle_Interface idle_iface = { idle };
static const LV2UI_Resize resize_ui = { 0, resize_func };

static const void*
extension_data(const char* uri)
{
  if (!strcmp(uri, LV2_UI__idleInterface))
  {
    return &idle_iface;
  }
  if (!strcmp(uri, LV2_UI__resize))
  {
    return &resize_ui;
  }
  return NULL;
}

static const LV2UI_Descriptor sustainUI_descriptor = {
    SUSTAINUI_URI,
    init_sustainUI,
    cleanup_sustainUI,
    sustainUI_port_event,
    extension_data
};


LV2_SYMBOL_EXPORT 
const LV2UI_Descriptor* lv2ui_descriptor(uint32_t index) 
{
    switch (index) {
    case 0:
        return &sustainUI_descriptor;
//    case 1:
//        return &stuckstackerUI_descriptor;
    default:
        return NULL;
    }
}

