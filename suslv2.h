#ifndef SUSLV2_H
#define SUSLV2_H


#define SUSTAINLV2_URI "http://gitlab.com/stazed/lv2-example#sustainer"
#define SUSTAINUI_URI "http://gitlab.com/stazed/lv2-example#sustainer_ui"


enum RKRLV2_ports_
{
    INL =0,
    INR,
    OUTL,
    OUTR,
    BYPASS,
    PARAM0,     // GAIN
    PARAM1,     // SUS
    PARAM2,
    PARAM3,
    PARAM4,
    PARAM5,
    PARAM6,
    PARAM7,
    PARAM8,
    PARAM9,
    PARAM10,
    PARAM11,
    PARAM12,
    PARAM13,
    PARAM14,
    PARAM15,
    PARAM16,
    PARAM17,
    PARAM18,
    DBG,
    EXTRA
};

/* These need to be in the same order as manifest.ttl, start with 0 for multiple plugins */
enum RKRLV2_effects_
{
    ISUS = 0
};


#endif
