
#include "sustain_ui.h"

SustainUI* self = 0;


int main()
{

    self = new SustainUI();
    Fl::scheme("gleam");
    self->show();

    return Fl::run();
}

// ntk
// g++ -g3 -ggdb -Wall -Wextra  -fPIC -DPIC sliderW.cxx sustain_ui.cxx main_test.cpp `pkg-config --cflags --libs ntk ` -o test.out
// fltk
// g++ -g3 -ggdb -Wall -Wextra  -fPIC -DPIC sliderW.cxx sustain_ui.cxx main_test.cpp -lfltk -o test.out
