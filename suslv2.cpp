#include<lv2.h>
#include<lv2/lv2plug.in/ns/ext/urid/urid.h>
#include<lv2/lv2plug.in/ns/ext/midi/midi.h>
#include<lv2/lv2plug.in/ns/ext/atom/util.h>
#include<lv2/lv2plug.in/ns/ext/atom/forge.h>
#include<lv2/lv2plug.in/ns/ext/time/time.h>
#include<lv2/lv2plug.in/ns/ext/buf-size/buf-size.h>
#include<lv2/lv2plug.in/ns/ext/options/options.h>
#include<lv2/lv2plug.in/ns/ext/atom/atom.h>
#include<lv2/lv2plug.in/ns/ext/patch/patch.h>
#include<lv2/lv2plug.in/ns/ext/worker/worker.h>
#include<lv2/lv2plug.in/ns/ext/state/state.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include"suslv2.h"

#include"sustainer.h"


//this is the default hopefully hosts don't use periods of more than this, or they will communicate the max bufsize
#define INTERMEDIATE_BUFSIZE 1024


typedef struct _RKRLV2
{
    uint8_t nparams;
    uint8_t effectindex;//index of effect
    uint16_t period_max;
    uint8_t loading_file;//flag to indicate that file load work is underway
    uint8_t file_changed;
	uint8_t prev_bypass;
    float	*tmp_l;//temporary buffers for wet/dry mixing for hosts with shared in/out buffers(Ardour)
    float 	*tmp_r;

    //ports
    float *input_l_p;
    float *input_r_p;
    float *output_l_p;
    float *output_r_p;
    float *bypass_p;
    const LV2_Atom_Sequence* atom_in_p;
    LV2_Atom_Sequence* atom_out_p;
    float *param_p[16];
    float *dbg_p;

    //various "advanced" lv2 stuffs
    LV2_Worker_Schedule* scheduler;
    LV2_Atom_Forge	forge;
    LV2_Atom_Forge_Frame atom_frame;
    LV2_URID_Map *urid_map;

    struct urids
    {
        LV2_URID    midi_MidiEvent;
        LV2_URID    atom_Float;
        LV2_URID    atom_Int;
        LV2_URID    atom_Object;
        LV2_URID    atom_Path;
        LV2_URID    atom_URID;
        LV2_URID    bufsz_max;
        LV2_URID    patch_Set;
        LV2_URID    patch_Get;
        LV2_URID	patch_property;
        LV2_URID 	patch_value;
        LV2_URID 	filetype_rvb;
        LV2_URID 	filetype_dly;

    } URIDs;

    //effect modules

    Sustainer* sus;	    // 0

} RKRLV2;


void getFeatures(RKRLV2* plug, const LV2_Feature * const* host_features)
{
    uint8_t i,j;
    plug->period_max = INTERMEDIATE_BUFSIZE;
    plug->loading_file = 0;
    plug->file_changed = 0;
    plug->scheduler = 0;
    plug->urid_map = 0;
    for(i=0; host_features[i]; i++)
    {
        if(!strcmp(host_features[i]->URI,LV2_OPTIONS__options))
        {
            LV2_Options_Option* option;
            option = (LV2_Options_Option*)host_features[i]->data;
            for(j=0; option[j].key; j++)
            {
                if(option[j].key == plug->URIDs.bufsz_max)
                {
                    if(option[j].type == plug->URIDs.atom_Int)
                    {
                        plug->period_max = *(const int*)option[j].value;
                    }
                    //other types?
                }
            }
        }
        else if(!strcmp(host_features[i]->URI,LV2_WORKER__schedule))
        {
            plug->scheduler = (LV2_Worker_Schedule*)host_features[i]->data;
        }
        else if(!strcmp(host_features[i]->URI,LV2_URID__map))
        {
            plug->urid_map = (LV2_URID_Map *) host_features[i]->data;
            if(plug->urid_map)
            {
                plug->URIDs.midi_MidiEvent = plug->urid_map->map(plug->urid_map->handle,LV2_MIDI__MidiEvent);
                plug->URIDs.atom_Float = plug->urid_map->map(plug->urid_map->handle,LV2_ATOM__Float);
                plug->URIDs.atom_Int = plug->urid_map->map(plug->urid_map->handle,LV2_ATOM__Int);
                plug->URIDs.atom_Object = plug->urid_map->map(plug->urid_map->handle,LV2_ATOM__Object);
                plug->URIDs.atom_Path = plug->urid_map->map(plug->urid_map->handle,LV2_ATOM__Path);
                plug->URIDs.atom_URID = plug->urid_map->map(plug->urid_map->handle,LV2_ATOM__URID);
                plug->URIDs.bufsz_max = plug->urid_map->map(plug->urid_map->handle,LV2_BUF_SIZE__maxBlockLength);
                plug->URIDs.patch_Set = plug->urid_map->map(plug->urid_map->handle,LV2_PATCH__Set);
                plug->URIDs.patch_Get = plug->urid_map->map(plug->urid_map->handle,LV2_PATCH__Get);
                plug->URIDs.patch_property = plug->urid_map->map(plug->urid_map->handle,LV2_PATCH__property);
                plug->URIDs.patch_value = plug->urid_map->map(plug->urid_map->handle,LV2_PATCH__value);
//                plug->URIDs.filetype_rvb = plug->urid_map->map(plug->urid_map->handle,RVBFILE_URI);
//                plug->URIDs.filetype_dly = plug->urid_map->map(plug->urid_map->handle,DLYFILE_URI);

            }
        }
    }
}

void
xfade_in (RKRLV2* plug, uint32_t period)
{
    unsigned int i;
    float v = 0;
    float step = 1/(float)period;
	//just a linear fade since it's (hopefully) correlated
    for (i = 0; i < period; i++)
    {
    	plug->output_l_p[i] = (v)*plug->output_l_p[i] + (1-v)*plug->input_l_p[i];
    	plug->output_r_p[i] = (v)*plug->output_r_p[i] + (1-v)*plug->input_r_p[i];
    	v+=step;
    }
}

void
xfade_out (RKRLV2* plug, uint32_t period)
{
    unsigned int i;
    float v = 0;
    float step = 1/(float)period;
	//just a linear fade since it's (hopefully) correlated
    for (i = 0; i < period; i++)
    {
    	plug->output_l_p[i] = (1-v)*plug->output_l_p[i] + v*plug->input_l_p[i];
    	plug->output_r_p[i] = (1-v)*plug->output_r_p[i] + v*plug->input_r_p[i];
    	v+=step;
    }
}

void
xfade_check (RKRLV2* plug, uint32_t period)
{
    if(*plug->bypass_p)
    {
    	plug->prev_bypass = 1;
    	xfade_out(plug,period);
    }
    else if(plug->prev_bypass)
    {
    	plug->prev_bypass = 0;
    	xfade_in(plug,period);
    }
}


///// Sustainer /////////
LV2_Handle init_suslv2(const LV2_Descriptor *descriptor,double sample_freq, const char *bundle_path,const LV2_Feature * const* host_features)
{
    RKRLV2* plug = (RKRLV2*)malloc(sizeof(RKRLV2));
    
    plug->nparams = 2;
    plug->effectindex = ISUS;
    plug->prev_bypass = 1;

    getFeatures(plug,host_features);

    plug->sus = new Sustainer(sample_freq, plug->period_max);

    return plug;
}

void run_suslv2(LV2_Handle handle, uint32_t nframes)
{
    if( nframes == 0)
        return;
    
    int i;
    int val;

    RKRLV2* plug = (RKRLV2*)handle;
    
    //inline copy input to output
    memcpy(plug->output_l_p,plug->input_l_p,sizeof(float)*nframes);
    memcpy(plug->output_r_p,plug->input_r_p,sizeof(float)*nframes);

    // are we bypassing
    if(*plug->bypass_p && plug->prev_bypass)
    {
        //printf("Got Bypass\n");
        plug->sus->cleanup();
        return;
    }
 
    /* adjust for possible variable nframes */
    if(plug->period_max != nframes)
    {
        plug->period_max = nframes;
        plug->sus->lv2_update_params(nframes);
    }
    
    
    /* Process incoming MIDI messages */
    LV2_ATOM_SEQUENCE_FOREACH(plug->atom_in_p , ev)
    {
        if (ev->body.type ==  plug->URIDs.midi_MidiEvent)
        {
            const uint8_t* const msg = (const uint8_t*)(ev + 1);
            switch (lv2_midi_message_type(msg))
            {
                case LV2_MIDI_MSG_CONTROLLER:
                {
                    printf("Got CC %d: Control Change %d: Value %d\n", msg[0], msg[1], msg[2]);
                    
                    /* CC 1 = Volume */
                    if(msg[1] == 1)
                    {
                        plug->sus->m_midi_control = true;   // if midi control then bypass plug->param_p[] - hack to stop param_p[] from changing it back.
                        plug->sus->changepar(0, msg[2]);
                    }
                    else
                    {
                        plug->sus->m_midi_control = false;
                    }

                       // ++self->n_active_notes;
                break;
                }
                case LV2_MIDI_MSG_PGM_CHANGE:
                        if (msg[1] == 0 || msg[1] == 1)
                        {
                               // self->program = msg[1];
                        }
                        break;
                default: break;
            }
        }
    }
    
    // we are good to run now
    //check and set changed parameters
    if(!plug->sus->m_midi_control)
    {
        for(i=0; i<plug->nparams; i++)
        {
            val = (int)*plug->param_p[i];
            if(plug->sus->getpar(i) != val)
            {
                printf("Change Par i = %d: val = %d\n", i, val);
                plug->sus->changepar(i,val);
            }
        }
    }
    //now run
    plug->sus->out(plug->output_l_p,plug->output_r_p);

    xfade_check(plug,nframes);
    return;
}

void cleanup_rkrlv2(LV2_Handle handle)
{
    RKRLV2* plug = (RKRLV2*)handle;
    switch(plug->effectindex)
    {
    case ISUS:
        delete plug->sus;
        break;

    }
    free(plug);
}

void connect_rkrlv2_ports_w_atom(LV2_Handle handle, uint32_t port, void *data)
{
    RKRLV2* plug = (RKRLV2*)handle;
    switch(port)
    {
    case INL:
        plug->input_l_p = (float*)data;
        break;
    case INR:
        plug->input_r_p = (float*)data;
        break;
    case OUTL:
        plug->output_l_p = (float*)data;
        break;
    case OUTR:
        plug->output_r_p = (float*)data;
        break;
    case BYPASS:
        plug->atom_in_p = (const LV2_Atom_Sequence*)data;
        break;
    case PARAM0:
        plug->atom_out_p = (LV2_Atom_Sequence*)data;
        break;
    case PARAM1:
        plug->bypass_p = (float*)data;
        break;
    case PARAM2:
        plug->param_p[0] = (float*)data;
        break;
    case PARAM3:
        plug->param_p[1] = (float*)data;
        break;
    case PARAM4:
        plug->param_p[2] = (float*)data;
        break;
    case PARAM5:
        plug->param_p[3] = (float*)data;
        break;
    case PARAM6:
        plug->param_p[4] = (float*)data;
        break;
    case PARAM7:
        plug->param_p[5] = (float*)data;
        break;
    case PARAM8:
        plug->param_p[6] = (float*)data;
        break;
    case PARAM9:
        plug->param_p[7] = (float*)data;
        break;
    case PARAM10:
        plug->param_p[8] = (float*)data;
        break;
    case PARAM11:
        plug->param_p[9] = (float*)data;
        break;
    case PARAM12:
        plug->param_p[10] = (float*)data;
        break;
    case PARAM13:
        plug->param_p[11] = (float*)data;
        break;
    case PARAM14:
        plug->param_p[12] = (float*)data;
        break;
    case PARAM15:
        plug->param_p[13] = (float*)data;
        break;
    case PARAM16:
        plug->param_p[14] = (float*)data;
        break;
    case PARAM17:
        plug->param_p[15] = (float*)data;
        break;
    case PARAM18:
        plug->param_p[16] = (float*)data;
        break;
    case DBG:
        plug->param_p[17] = (float*)data;
        break;
    case EXTRA:
        plug->param_p[18] = (float*)data;
        break;
    default:
        puts("UNKNOWN PORT YO!!");
    }
}

void connect_rkrlv2_ports(LV2_Handle handle, uint32_t port, void *data)
{
    RKRLV2* plug = (RKRLV2*)handle;
    switch(port)
    {
    case INL:
        plug->input_l_p = (float*)data;
        break;
    case INR:
        plug->input_r_p = (float*)data;
        break;
    case OUTL:
        plug->output_l_p = (float*)data;
        break;
    case OUTR:
        plug->output_r_p = (float*)data;
        break;
    case BYPASS:
        plug->bypass_p = (float*)data;
        break;
    case PARAM0:
        plug->param_p[0] = (float*)data;
        break;
    case PARAM1:
        plug->param_p[1] = (float*)data;
        break;
    case PARAM2:
        plug->param_p[2] = (float*)data;
        break;
    case PARAM3:
        plug->param_p[3] = (float*)data;
        break;
    case PARAM4:
        plug->param_p[4] = (float*)data;
        break;
    case PARAM5:
        plug->param_p[5] = (float*)data;
        break;
    case PARAM6:
        plug->param_p[6] = (float*)data;
        break;
    case PARAM7:
        plug->param_p[7] = (float*)data;
        break;
    case PARAM8:
        plug->param_p[8] = (float*)data;
        break;
    case PARAM9:
        plug->param_p[9] = (float*)data;
        break;
    case PARAM10:
        plug->param_p[10] = (float*)data;
        break;
    case PARAM11:
        plug->param_p[11] = (float*)data;
        break;
    case PARAM12:
        plug->param_p[12] = (float*)data;
        break;
    case PARAM13:
        plug->param_p[13] = (float*)data;
        break;
    case PARAM14:
        plug->param_p[14] = (float*)data;
        break;
    case PARAM15:
        plug->param_p[15] = (float*)data;
        break;
    case PARAM16:
        plug->param_p[16] = (float*)data;
        break;
    case PARAM17:
        plug->param_p[17] = (float*)data;
        break;
    case PARAM18:
        plug->param_p[18] = (float*)data;
        break;
    case DBG:
        plug->dbg_p = (float*)data;
        break;
    default:
        puts("UNKNOWN PORT YO!!");
    }
}


static const LV2_Descriptor suslv2_descriptor=
{
    SUSTAINLV2_URI,
    init_suslv2,
    connect_rkrlv2_ports_w_atom,
//    connect_rkrlv2_ports,
    0,//activate
    run_suslv2,
    0,//deactivate
    cleanup_rkrlv2,
    0//extension
};

LV2_SYMBOL_EXPORT
const LV2_Descriptor* lv2_descriptor(uint32_t index)
{
    switch (index)
    {
    case ISUS:
        return &suslv2_descriptor ;
    default:
        return 0;
    }
}

