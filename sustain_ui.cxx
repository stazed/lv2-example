// generated by Fast Light User Interface Designer (fluid) version 1.0303

#include "sustain_ui.h"

void SustainUI::cb_sus_activar_i(Fl_Button* o, void*) {
  float tmp = o->value();
if(tmp == 0.0)
{
  tmp = 1.0;
  o->label("Off");
}
else
{
  tmp = 0.0;
  o->label("On");
}
write_function(controller,PARAM1,sizeof(float),0,&tmp);
}
void SustainUI::cb_sus_activar(Fl_Button* o, void* v) {
  ((SustainUI*)(o->parent()->parent()->user_data()))->cb_sus_activar_i(o,v);
}

void SustainUI::cb_sus_gain_i(SliderW* o, void*) {
  float tmp = o->value();
write_function(controller,PARAM2,sizeof(float),0,&tmp);
}
void SustainUI::cb_sus_gain(SliderW* o, void* v) {
  ((SustainUI*)(o->parent()->parent()->user_data()))->cb_sus_gain_i(o,v);
}

void SustainUI::cb_sus_sus_i(SliderW* o, void*) {
  float tmp = o->value();
write_function(controller,PARAM3,sizeof(float),0,&tmp);
}
void SustainUI::cb_sus_sus(SliderW* o, void* v) {
  ((SustainUI*)(o->parent()->parent()->user_data()))->cb_sus_sus_i(o,v);
}

Fl_Double_Window* SustainUI::show() {
  { Fl_Double_Window* o = ui = new Fl_Double_Window(155, 180, "Example Sustain");
    ui->box(FL_UP_BOX);
    ui->user_data((void*)(this));
    { bg = new Fl_Group(0, 0, 155, 181);
      bg->box(FL_UP_BOX);
      bg->color(FL_GRAY0);
      { Fl_Button* o = sus_activar = new Fl_Button(5, 4, 34, 18, "On");
        sus_activar->down_box(FL_DOWN_BOX);
        sus_activar->color((Fl_Color)178);
        sus_activar->selection_color(FL_GREEN);
        sus_activar->labelcolor(FL_GRAY0);
        sus_activar->callback((Fl_Callback*)cb_sus_activar);
        sus_activar->align(Fl_Align(FL_ALIGN_CLIP|FL_ALIGN_INSIDE));
        sus_activar->when(FL_WHEN_CHANGED);
        o->type(1);
      } // Fl_Button* sus_activar
      { sus_gain = new SliderW(49, 60, 100, 15, "Gain");
        sus_gain->type(5);
        sus_gain->box(FL_UP_BOX);
        sus_gain->color((Fl_Color)178);
        sus_gain->selection_color((Fl_Color)62);
        sus_gain->labeltype(FL_NORMAL_LABEL);
        sus_gain->labelfont(1);
        sus_gain->labelsize(10);
        sus_gain->labelcolor(FL_BACKGROUND2_COLOR);
        sus_gain->maximum(127);
        sus_gain->step(1);
        sus_gain->textfont(1);
        sus_gain->textcolor(FL_BACKGROUND2_COLOR);
        sus_gain->callback((Fl_Callback*)cb_sus_gain);
        sus_gain->align(Fl_Align(FL_ALIGN_LEFT));
        sus_gain->when(FL_WHEN_CHANGED);
      } // SliderW* sus_gain
      { sus_sus = new SliderW(49, 86, 100, 15, "Sustain");
        sus_sus->type(5);
        sus_sus->box(FL_UP_BOX);
        sus_sus->color((Fl_Color)178);
        sus_sus->selection_color((Fl_Color)62);
        sus_sus->labeltype(FL_NORMAL_LABEL);
        sus_sus->labelfont(1);
        sus_sus->labelsize(10);
        sus_sus->labelcolor(FL_BACKGROUND2_COLOR);
        sus_sus->minimum(1);
        sus_sus->maximum(127);
        sus_sus->step(1);
        sus_sus->value(32);
        sus_sus->textfont(1);
        sus_sus->textcolor(FL_BACKGROUND2_COLOR);
        sus_sus->callback((Fl_Callback*)cb_sus_sus);
        sus_sus->align(Fl_Align(FL_ALIGN_LEFT));
        sus_sus->when(FL_WHEN_CHANGED);
      } // SliderW* sus_sus
      bg->end();
      Fl_Group::current()->resizable(bg);
    } // Fl_Group* bg
    o->show();
    ui->end();
  } // Fl_Double_Window* ui
  return ui;
}

void SustainUI::idle() {
  Fl::check();
  Fl::flush();
}
