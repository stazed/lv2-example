This is a lv2 example project to be used for testing various custom guis.

The simple sustainer plugin was grabbed from the rakarrack/rkrlv2 project.

To compile: make

Will create a lv2 bundle: lv2-example-sustain.lv2 
